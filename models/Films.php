<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "films".
 *
 * @property integer $id
 * @property string $name
 * @property integer $year
 * @property integer $isActive
 */
class Films extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'films';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'year'], 'required'],
            [['year', 'isActive'], 'integer'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'year' => 'Year',
            'isActive' => 'Is Active',
        ];
    }
}
